import os

SOCKET_PORT = int(os.getenv("SOCKET_PORT",8801))
SOCKET_HOST = os.getenv("SOCKET_HOST","0.0.0.0")