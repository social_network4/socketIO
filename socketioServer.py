import socket
import logging
import requests
import time
import datetime
import socketio
import pynmea2
import json
import pytz
import eventlet
from _thread import *
from flask import Flask
import setting


log_fmt = logging.Formatter(fmt="%(asctime)s %(name)s: %(message)s", datefmt='%Y-%m-%d %H:%M:%S')
log_handler = logging.StreamHandler()
log_handler.setFormatter(log_fmt)
log = logging.getLogger('Flask')
log.handlers = []
log.addHandler(log_handler)
log.propagate = False

sio = socketio.Server(async_handlers='threading')
app = Flask(__name__)
app.wsgi_app = socketio.WSGIApp(sio, app.wsgi_app)

USER_LIST = []
"""
{
    "userId":"abc123",
    "socketId":"aaaa"
}
"""

@sio.event
def connect(sid, environ):
    print(sid, "connected")
    log.debug("connected",sid)
    # USER_ID.append(sid)

@sio.event
def registry(sid, data:dict = {}):
    print(sid, "connected")
    log.debug("registry",data)
    USER_LIST.append({"userId":data.get("userId",""),"socketId":str(sid)})
    sio.emit("registry",data=sid,to=sid)


@sio.on("message_send")
def message_send(sid,data):
    log.debug("message_send",sid,data)
    receiveID = data.get("receiveID","")
    content = data.get("content","")
    senderID = data.get("senderID","")
    listReceiver = list(filter(lambda x: x["userId"] == receiveID,USER_LIST))
    for user in listReceiver:
        sio.emit(event="message_listen",data={"senderID":senderID,"content":content},to=user["socketId"])


@sio.event
def disconnect(sid):
    print(sid, "disconnect")
    log.debug(sid, "disconnect")
    global USER_LIST
    USER_LIST = list(filter(lambda x:x["socketId"]==sid,USER_LIST))


def startSkIO():
    eventlet.wsgi.server(eventlet.listen((setting.SOCKET_HOST, setting.SOCKET_PORT)), app)


if __name__ == '__main__':
    startSkIO()
    sio.emit("requestRegist", "", broadcast=True)
    print("socket READY")
    log.debug("socket READY")
